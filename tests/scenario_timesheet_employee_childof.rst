===================================
Timesheet Employee Childof Scenario
===================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install timesheet_employee_childof::

    >>> config = activate_modules('timesheet_employee_childof')
