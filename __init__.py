# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import timesheet
from . import employee_team
from . import company


def register():
    Pool.register(
        timesheet.Timesheet,
        company.Company,
        module='timesheet_employee_childof', type_='model')
    Pool.register(
        timesheet.TeamTimesheetEmployee,
        timesheet.TeamTimesheetWork,
        module='timesheet_employee_childof', type_='model',
        depends=['team_timesheet'])
    Pool.register(
        employee_team.TeamEmployee,
        module='timesheet_employee_childof', type_='model',
        depends=['company_employee_team'])
