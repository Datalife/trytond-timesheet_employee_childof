# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Timesheet', 'TeamTimesheetEmployee', 'TeamTimesheetWork']


class Timesheet(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    @classmethod
    def __setup__(cls):
        super(Timesheet, cls).__setup__()
        cls.work.domain = [
            ('company', 'parent_of', Eval('company'), 'parent')
        ] + cls.work.domain[1:]


class TeamTimesheetEmployee(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-company.employee'

    @classmethod
    def __setup__(cls):
        super(TeamTimesheetEmployee, cls).__setup__()
        cls.employee.domain = [
            ('company', 'child_of', Eval('_parent_team_timesheet',
                {}).get('company', None), 'parent')
        ] + cls.employee.domain[1:]


class TeamTimesheetWork(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work'

    def _get_timesheet(self, Timesheet, employee, hours):
        res = super(TeamTimesheetWork, self)._get_timesheet(Timesheet,
            employee, hours)
        res.company = employee.company
        return res
