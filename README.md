datalife_timesheet_employee_childof
===================================

The timesheet_employee_childof module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-timesheet_employee_childof/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-timesheet_employee_childof)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
