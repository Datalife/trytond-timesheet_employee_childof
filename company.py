# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    parent = fields.Many2One('company.company', 'Parent',
        help="Add the company below the parent.")
    childs = fields.One2Many('company.company', 'parent', 'Children',
        help="Add children below the company.")
